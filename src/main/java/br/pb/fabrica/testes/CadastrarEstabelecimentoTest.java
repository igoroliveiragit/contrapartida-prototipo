package br.pb.fabrica.testes;

import br.pb.fabrica.core.BaseTest;
import br.pb.fabrica.core.DriverFactory;
import br.pb.fabrica.pages.CadastrarEstabelecimentoPage;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class CadastrarEstabelecimentoTest extends BaseTest {

    WebDriver driver = DriverFactory.getDriver();

    private CadastrarEstabelecimentoPage page = new CadastrarEstabelecimentoPage();

    @Test
    public void testCriarEstabelecimento() {
        driver.get("https://contra-partida.herokuapp.com/gestao/createestabelecimento/");
        page.setTipoDeEstabelecimento("Unidade de saúde");
        page.clicarBotaoEnviar();
        Assert.assertEquals("Estabelecimento cadastrado com sucesso", page.obterTexto("xpath da mensagem aqui"));
    }

    @Test
    public void testCriarEstabelecimentoJaExistente() {
        driver.get("https://contra-partida.herokuapp.com/gestao/createestabelecimento/");
        page.setTipoDeEstabelecimento("Unidade de saúde");
        page.clicarBotaoEnviar();
        Assert.assertEquals("Estabelecimento com este Tipo já existe.", page.obterTexto("//*[@id=\"content\"]/div[2]/form/div/div/div/div/div/span"));
    }
}
