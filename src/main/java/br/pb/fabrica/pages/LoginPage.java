package br.pb.fabrica.pages;

import br.pb.fabrica.core.BasePage;
import br.pb.fabrica.core.DriverFactory;
import org.openqa.selenium.By;

public class LoginPage extends BasePage {

    public void acessarPaginaLogin(){
        DriverFactory.getDriver().get("https://contra-partida.herokuapp.com/");
    }

    public void setUsuario(String nome){
        escrever("id_username", nome);
    }

    public void setSenha(String senha){
        escrever("id_password", senha);
    }

    public void clicarBotaoEntrar(){
        clicarBotao("//button[text()=\"Entrar\"]");
    }

}
