package br.pb.fabrica.pages;

import br.pb.fabrica.core.BasePage;
import br.pb.fabrica.core.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public abstract class ListaGenericaPage extends BasePage {

    public WebElement buscarTabela(){return DriverFactory.getDriver().findElement(By.className("table")); }

    public void setBuscar(String texto){ escrever(By.xpath("//input[@placeholder='Procurar por...']"), texto);}

    public void clicarBotaoBuscar(){ clicarBotao("//*[contains(@class, 'fas fa-search fa-sm')]"); };

    public abstract void clicarBotaoCadastrarNovo();

    public int buscarElementoTabela(String texto, int idColuna){return obterIndiceLinha(texto, buscarTabela(), idColuna);}

}
