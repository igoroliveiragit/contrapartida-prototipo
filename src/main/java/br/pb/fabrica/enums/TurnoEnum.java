package br.pb.fabrica.enums;

public enum TurnoEnum {

	Manhã,
	Tarde,
	Noite,
	Integral
}
